import React from "react";
import Sale from "./Sale";

class ProductList extends React.Component {
  render() {
    return (
      <>
        <div>
          {this.props.lista.map((elemento, index) => (
            <div key={index}>
              {elemento.name}
              <Sale
                filho={
                  elemento.discountPercentage
                    ? elemento.discountPercentage
                    : "0"
                }
              />
            </div>
          ))}
        </div>
      </>
    );
  }
}

export default ProductList;
